json.(@player, :id, :full_name, :age, :height_in_feet, :weight_in_pounds)

if @player.events.present?
  json.events do
    @player.events_structure.each do |action, events|
      json.set! action, events, :id, :minute
    end
  end

  json.(@player, :avg_pos_x, :avg_pos_y)
end