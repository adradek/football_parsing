require 'nokogiri'



class PlayersParser

  def self.perform(doc)

    xml = Nokogiri::XML(doc)

    players = xml.xpath("//player")
    events  = xml.xpath("//event")

    players.each do |p|
      stored_player = Player.find_by(id: p["id"])
      if stored_player
        stored_player.update!(player_to_hash(p))
      else
        Player.create!(player_to_hash(p))
      end
    end

    events.each do |e|
      # next unless Player.find_by(id: e["player_id"])
      stored_event = Event.find_by(id: e["id"])
      if stored_event
        stored_event.update!(event_to_hash(e))
      else
        Event.create!(event_to_hash(e))
      end
    end

  end

  def self.player_to_hash(p)
    {
      id:       p["id"],   # will be ignored in the case of update
      name:     p["name"],
      surname:  p["surname"],
      birthday: p["birthday"].to_date,
      height:   p["height"],
      weight:   p["weight"]
    }
  end

  def self.event_to_hash(e)
    {
      id:        e["id"],
      action:    e["action"],
      player_id: e["player_id"],
      half:      e["half"],
      second:    e["second"],
      pos_x:     (e["pos_x"].gsub(",", ".").to_f * 10).round,
      pos_y:     (e["pos_y"].gsub(",", ".").to_f * 10).round
    }
  end
end