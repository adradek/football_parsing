class PlayersController < ApplicationController
  
  def incoming
    if request.content_type != Mime::XML
      render text: "Error", status: 404
      return
    end

    #PlayersParseWorker.perform_async(request.body)
    PlayersParser.perform(request.body)
    render text: "OK"
  end


  def show
    @player = Player.includes(:events).find(params[:id])
  end

end
