class Event < ActiveRecord::Base
  belongs_to :player

  def minute
    min = 1 + second.div(60)
    
    case half
      when "2"      then min += 45
      when "extra1" then min += 90
      when "extra2" then min += 105
      when "pen"    then min = 120
      else min
    end
  end
end
