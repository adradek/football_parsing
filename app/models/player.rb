require 'babosa'

class Player < ActiveRecord::Base
  include ActionView::Helpers::TextHelper

  has_many :events


  def full_name
    [name, surname].join(' ').to_slug.transliterate(:russian).to_s
  end


  def age
    today = Date.today
    
    if today.yday >= birthday.yday
      today.year - birthday.year
    else
      today.year - birthday.year - 1
    end
  end


  def height_in_feet
    inches = height.div 2.54
    foots  = inches.div 12
    rest   = inches % 12

    "#{foots} feet " + pluralize(rest, "inch")
  end


  def weight_in_pounds
    pluralize(weight.div(0.4536), 'pound')
  end


  def events_structure
    unless @events_structure
      actions = events.select(:action).uniq
      @events_structure = {}
      actions.each do |a|
        @events_structure[a.action] = events.where(action: a.action)
      end
    end
    
    @events_structure
  end

  def avg_pos_x
    avg(events.map(&:pos_x)).round(1)
  end

  def avg_pos_y
    avg(events.map(&:pos_y)).round(1)
  end

  private
    def avg(array)
      array.sum.to_f/(array.size * 10)
    end
end
