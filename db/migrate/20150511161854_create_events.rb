class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :player_id, null: false
      t.column  :action, "enum('goal','free_kick','corner','yellow_card','red_card','foul','in','out','save','throw_in','penalty_scored','penalty_fail','penalty_save')"
      t.column  :half,   "enum('1','2','extra1','extra2','pen')"
      t.integer :second, limit: 2
      t.integer :pos_x,  limit: 2
      t.integer :pos_y,  limit: 2

      t.timestamps null: false
    end

    add_index :events, :player_id
  end
end
